+++
title = "home"
paginate_by = 5
+++

Theological resources for Catholics.

# Topics

- [Sacraments of the Church](/topics/sacraments)

- [The Mass, what it's for, and what it means](/topics/mass)

- [Fasting](/topics/fasting)


# Blessed Virgin Mary

- [Ever Virgin](/mary/ever-virgin)


# Catechisms

- [The catechisms of the Catholic Church](/topics/catechisms)


# Scripture, letters, encyclicals, and other documents

- [Septuagint](/documents/protoevangelium-of-james)

- [Protoevangelium of James](/documents/protoevangelium-of-james)


# People

- [Philo](/people/philo)
