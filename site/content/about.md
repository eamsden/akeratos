+++
title = "About"

[extra]
notitle = true
+++

## Catholic resources for the rest of us

I've found websites like [churchfathers.org](https://churchfathers.org) helpful but I wanted a more compehensive, topically-oriented collection of Catholic answers to theological questions. I have some friends helping me (Chris Allen) with this website but any errors are mine. I don't have imprimatur or authority from the Church to teach. Always defer to and obey your local ordinary on any specific matters!

## Why Lorepub?

Functionally it seemed to dovetail with my mission in starting Lorepub: preserving, organizing, and spreading knowledge. This particular site just happens to be about my faith.

## First things

Something I want to impress upon any readers: learning is salutary and apologetics is good but attending Mass, having a regular prayer life, and a relationship with the people and clergy of your parish should come first!