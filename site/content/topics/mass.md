+++
title = "Holy Sacrifice of the Mass"
+++

<!-- - [Bishop Barron on the Mass](https://www.youtube.com/watch?v=pIGXtDR2GCk) -->

# Purpose of the Mass

<!-- The four ends, or purposes, of the Mass: -->

<!-- ## Adoration of God -->

Adoration, or worship, is the first and foremost purpose of the Mass.

[CC 1330](http://www.vatican.va/archive/ccc_css/archive/catechism/p2s2c1a3.htm)

>The Holy Sacrifice, because it makes present the one sacrifice of Christ the Savior and includes the Church's offering. The terms holy sacrifice of the Mass, "sacrifice of praise," spiritual sacrifice, pure and holy sacrifice are also used,150 since it completes and surpasses all the sacrifices of the Old Covenant.

[CC 1378](http://www.vatican.va/archive/ccc_css/archive/catechism/p2s2c1a3.htm)

>In the liturgy of the Mass we express our faith in the real presence of Christ under the species of bread and wine by, among other ways, genuflecting or bowing deeply as a sign of adoration of the Lord. "The Catholic Church has always offered and still offers to the sacrament of the Eucharist the cult of adoration, not only during Mass, but also outside of it, reserving the consecrated hosts with the utmost care, exposing them to the solemn veneration of the faithful, and carrying them in procession."208

[CC 1382](http://www.vatican.va/archive/ccc_css/archive/catechism/p2s2c1a3.htm)

>The Mass is at the same time, and inseparably, the sacrificial memorial in which the sacrifice of the cross is perpetuated and the sacred banquet of communion with the Lord's body and blood. But the celebration of the Eucharistic sacrifice is wholly directed toward the intimate union of the faithful with Christ through communion. To receive communion is to receive Christ himself who has offered himself for us.

<!-- ## Thanksgiving to God -->

<!-- ## Reparation to God -->

<!-- ## Petition to God -->

CC 1088, 1332, 1832, 2192
1068

## The four ends of the Mass

[From this PDF document at stspeterandpaulbasilica.com](https://www.stspeterandpaulbasilica.com/files/catechesis/BasilicaHandout_FourEndsOfTheMass.pdf)

### Adoration

The sacrifice of God's only Son is the only truly worthy gift we can offer Him in honor and adoration.

### Atonement

Through the sacrifice of the Mass, we apply the Blood of Christ for the forgiveness of our sins.

### Thanksgiving

Through Christ we offer most efficiently our thanks and praise for all the many benefits we receive.

### Petition

Through Christ's intercession we hope to obtain the assistance of God's grace for our intentions.

<!-- # What can Mass be offered for? -->

<!-- [From the Baltimore Catechism](http://www.baltimore-catechism.com/lesson24.htm) -->

<!-- >Mass may be offered for any end or intention that tends to the honor and glory of God, to the good of the Church or the welfare of man; but never for any object that is bad in itself, or in its aims; neither can it be offered publicly for persons who are not members of the true Church. -->

<!--
  Ed. Not sure about this one
  Follow-up: given Nostra Aetate and the general practice of the Church,
  this doesn't really hold true any more in the practical sense even if
  it's theologically true.
-->

# What am I supposed to do?

[From the Baltimore Catechism](http://www.baltimore-catechism.com/lesson24.htm)

>Orate Fratres," etc., which means: "Pray, brethren, that my sacrifice and yours may be acceptable to God the Father Almighty," and the server answers in our name: "May the Lord receive the sacrifice from thy hands to the praise and glory of His own name, and to our benefit and that of all His Holy Church."

>We should assist at Mass with great interior recollection and piety and with every outward mark of respect and devotion.

>The best manner of hearing Mass is to offer it to God with the priest for the same purpose for which it is said, to meditate on Christ's sufferings and death, and to go to Holy Communion.

# The Sunday obligation

From [CC 2180](http://www.vatican.va/archive/ccc_css/archive/catechism/p3s2c1a3.htm)

>The precept of the Church specifies the law of the Lord more precisely: "On Sundays and other holy days of obligation the faithful are bound to participate in the Mass."117 "The precept of participating in the Mass is satisfied by assistance at a Mass which is celebrated anywhere in a Catholic rite either on the holy day or on the evening of the preceding day."118

# Parts of the Mass

Per the [USCCB's web page on the Order of Mass](http://www.usccb.org/prayer-and-worship/the-mass/order-of-mass/index.cfm)

## Introductory Rites

- Entrance
- Greeting
- Penitential Act
- Glory to God
- Collect

## Liturgy of the Word

- First Reading
- Responsorial Psalm
- Second Reading (on Sundays and solemnities)
- Gospel Acclamation
- Gospel
- Homily
- Profession of Faith (on Sundays, solemnities, and special occasions)
- Universal Prayer

## Liturgy of the Eucharist

- Presentation of the Gifts and Preparation of the Altar
- Prayer over the Offerings
- Eucharistic Prayer
- Preface
- Holy, Holy, Holy
- First half of prayer, including Consecration
- Mystery of Faith
- Second half of prayer, ending with Doxology
- The Lord's Prayer
- Sign of Peace
- Lamb of God
- Communion
- Prayer after Communion

## Concluding Rites

- Optional announcements
- Greeting and Blessing
- Dismissal

>Another way of dividing a Mass is into its "ordinary" parts -- those texts which, with some variations, are part of the Mass on a daily basis -- and its "proper" parts -- the texts of prayers and selection of Scripture readings proper to the specific feast, feria or other occasion being observed.

# How to pray the Mass

Don't pray _at_ Mass, _pray the Mass._

[Westminster Youth Ministry website on Praying the Mass](https://dowym.com/discover/praying-mass/)

Some excerpts,

>We can prepare well for a particular Mass by going to Confession and by arriving in good time to pray before Mass. If receiving Communion, we must abstain for at least one hour beforehand from all food and drink except water and medicine.

>We can participate well at Mass generally by being attentive and by uniting our own interior prayers to the words, actions and gestures of the priest. It is also important to: respond and sing clearly; listen carefully; try to understand what is happening; maintain prayerful silence; receive the Eucharist with the greatest reverence and genuflect with respect.

>It is good to spend a few minutes of prayer in church after Mass. This is a way of thanking God for his blessings. It also enables us to be mindful of the resolutions we have made so that the power of the sacrament will be fruitful in our lives.

>If conscious of grave sin, we should not receive the Communion without first going to sacramental Confession. However, to attend Mass devoutly has great value even when we cannot receive Communion. We may still make a ‘spiritual communion’.

<!-- He complains about Vatican II here, unfortunate as he's a brilliant writer and catechist. -->

<!-- [Father John A. Hardon, S.J. on Praying the Mass](http://www.therealpresence.org/archives/Mass/Mass_007.htm) -->

<!-- Some excerpts from Fr. Hardon's article: -->

## Difficulties paying attention at Mass?

[The Catholic Company has some good preparatory tips](https://www.catholiccompany.com/getfed/9-tips-focus-mass-6132)


<!-- Ordinary form / Extraordinary form should be about the same? -->
