.PHONY: clean build deploy prepare-staging build-staging remove-staging serve build-zola-docker push-image-to-gl install-zola

.DEFAULT_GOAL = help

SITE_DIR=site

ifdef CI_COMMIT_REF_NAME
    export BRANCH = $(CI_COMMIT_REF_NAME)
else 
    export BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
endif

export STAGING_HOST = staging.akeratos.com
STAGING_BUCKET=s3://staging.akeratos.com/
SITE_BUCKET=s3://akeratos.com/
DOCKER_REPO=registry.gitlab.com/lorepub/akeratos
ZOLA_VERSION=v0.6.0

## Wipe the cached site state clean
clean:
	$(MAKE) -C $(SITE_DIR) clean

## Build the site
build:
	$(MAKE) -C $(SITE_DIR) build

## Deploy the site to production
deploy: build
	aws s3 sync target $(SITE_BUCKET)

## Prepare staging
prepare-staging:
	aws s3 sync staging $(STAGING_BUCKET)

## Build staging
build-staging:
	$(MAKE) -C $(SITE_DIR) build-staging

## Deploy staging
deploy-staging: build-staging
	aws s3 sync target $(STAGING_BUCKET)$(BRANCH)

## Nuke staging
remove-staging:
	if [ "$(BRANCH)" = master ]; then \
	    echo "will not remove on master"; \
	else \
	    aws s3 rm --recursive $(STAGING_BUCKET)$(BRANCH); \
	fi

## Serve the local site
serve:
	$(MAKE) -C $(SITE_DIR) serve

## Build Zola Docker image
build-zola-docker:
	docker login registry.gitlab.com
	docker build -t $(DOCKER_REPO) .

## Push Docker image to GitLab
push-image-to-gl: build-zola-docker
	docker push $(DOCKER_REPO)

## Download the Zola executable and put it in your path
install-zola:
	wget https://github.com/getzola/zola/releases/download/$(ZOLA_VERSION)/zola-$(ZOLA_VERSION)-x86_64-unknown-linux-gnu.tar.gz -O ~/Downloads/zola.tar.gz
	mkdir -p ~/.bin/zola-$(ZOLA_VERSION)
	tar -xzf ~/Downloads/zola.tar.gz -C ~/.bin/zola-$(ZOLA_VERSION)/
	ln -s ~/.bin/zola-$(ZOLA_VERSION)/zola ~/.bin/zola

help:
	@echo "Please use \`make <target>' where <target> is one of\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-30s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
